package io.techniktom;

import io.techniktom.card.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    private final List<Card> cards;
    private final List<Card> remainingCards;
    private final List<Card> placedCards;

    public Deck() {
        cards = Collections.unmodifiableList(Card.Cards.getAllCards());
        remainingCards = new ArrayList<>(cards);
        placedCards = new ArrayList<>();
    }

    public void setupDeck() {
        placedCards.clear();
        remainingCards.clear();
        remainingCards.addAll(cards);

        Collections.shuffle(remainingCards);
    }

    public Card retrieveCard() {
        if(!remainingCards.isEmpty()) {
            return remainingCards.remove(0);
        }

        reroll();
        return retrieveCard();
    }

    public List<Card> retrieveCards(int count) {
        var array = new ArrayList<Card>();

        for (int i = 0; i < count; i++) {
            array.add(retrieveCard());
        }

        return array;
    }

    private void reroll() {
        remainingCards.addAll(placedCards);
        Collections.reverse(remainingCards);

        placedCards.clear();
    }

    public void addPlacedCard(Card card) {
        placedCards.add(card);
    }
}
