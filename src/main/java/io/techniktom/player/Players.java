package io.techniktom.player;

import java.util.*;

public class Players {
    private final Set<Player> players;
    private final Set<Player> remainingPlayers;
    private final Queue<Player> winners;

    public Players(Set<Player> players) {
        this.players = Collections.unmodifiableSet(players);
        this.remainingPlayers = new HashSet<>(players);
        this.winners = new ArrayDeque<>();

        if(players.size() < 2) {
            throw new RuntimeException("Not Enough players to play this game!");
        }
    }

    public void clearPlayersDecks() {
        players.forEach(Player::clearDeck);
    }

    public Set<Player> getRemainingPLayers() {
        return Set.copyOf(remainingPlayers);
    }

    public Integer getRemainingPlayersCount() {
        return remainingPlayers.size();
    }

    public void playerEnded(Player player) {
        if(remainingPlayers.contains(player)) {
            remainingPlayers.remove(player);

            winners.add(player);
        }
    }

    public Queue<Player> getWinners() {
        return winners;
    }

    public Set<Player> getAllPlayers() {
        return players;
    }

    public boolean isEnd() {
        return getRemainingPlayersCount() == 1;
    }
}
