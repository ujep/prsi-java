package io.techniktom;

import io.techniktom.card.CardColor;
import io.techniktom.card.CardType;
import io.techniktom.interfaces.GameInterface;
import io.techniktom.player.Player;
import io.techniktom.player.Players;

import java.util.Objects;
import java.util.Set;

public class Game implements Runnable {
    private static final Integer CARDS_PER_PLAYER = 4;

    private final Deck deck;
    private final Players players;
    private final GameInterface gameInterface;
    private CardColor colorOnChanger;
    private Integer overChargeCount;

    private int round;
    private boolean gameRunning;

    public Game(Set<Player> players, GameInterface gameInterface) {
        this.players = new Players(players);
        this.gameInterface = gameInterface;

        deck = new Deck();

        gameRunning = false;
        round = 0;
    }

    public void prepareGame() {
        deck.setupDeck();
        players.clearPlayersDecks();

        for (int i = 0; i < CARDS_PER_PLAYER; i++) {
            for (Player player : players.getAllPlayers()) {
                player.addCardToDeck(deck.retrieveCard());
            }
        }
    }

    public void startGame() {
        if(!gameRunning) {
            gameRunning = true;
            new Thread(this).start();
        }
    }

    @Override
    public void run() {
        gameInterface.gameStart();
        var currentCard = deck.retrieveCard();

        if(currentCard.getType() == CardType.MENIC) {
            colorOnChanger = currentCard.getColor();
        } else if(currentCard.getType() == CardType.ESO) {
            overChargeCount = 0;
        } else if(currentCard.getType() == CardType.SEVEN) {
            overChargeCount = 1;
        }

        gameInterface.showFirstCard(currentCard);

        while(players.getRemainingPlayersCount() > 1) {
            for (Player player : players.getRemainingPLayers()) {
                gameInterface.currentPlayer(player);

                if(currentCard.isSpecial() && overChargeCount != null) {
                    var optionalSelectedCard = gameInterface.letPlayerSelectCard(player, currentCard, null, overChargeCount);

                    if(optionalSelectedCard.isPresent()) {
                        var selectedCard = optionalSelectedCard.get();

                        if(Objects.equals(selectedCard.getType(), CardType.SEVEN)) {
                            overChargeCount++;
                        }

                        player.removeCardFromDeck(selectedCard);
                        deck.addPlacedCard(selectedCard);
                        gameInterface.placeCard(selectedCard);
                        currentCard = selectedCard;

                        if(!player.isPlaying()) {
                            players.playerEnded(player);
                        }
                    }else{
                        if(Objects.equals(currentCard.getType(), CardType.SEVEN)) {
                            deck
                                .retrieveCards(2*overChargeCount)
                                .forEach(player::addCardToDeck);

                            gameInterface.playerRetrievedOverChargedCards(player, 2*overChargeCount);
                        }

                        overChargeCount = null;
                    }
                }else{
                    var optionalSelectedCard = gameInterface.letPlayerSelectCard(player, currentCard, colorOnChanger, overChargeCount);

                    if(optionalSelectedCard.isPresent()) {
                        var selectedCard = optionalSelectedCard.get();

                        if(Objects.equals(selectedCard.getType(), CardType.MENIC)) {
                            colorOnChanger = gameInterface.letPlayerSelectColor();
                        } else if(Objects.equals(selectedCard.getType(), CardType.SEVEN)) {
                            overChargeCount = 1;
                        } else if (Objects.equals(selectedCard.getType(), CardType.ESO)) {
                            overChargeCount = 0;
                        }

                        player.removeCardFromDeck(selectedCard);
                        deck.addPlacedCard(selectedCard);
                        gameInterface.placeCard(selectedCard);
                        currentCard = selectedCard;

                        if(!player.isPlaying()) {
                            players.playerEnded(player);
                        }
                    }else{
                        var retrievedCard = deck.retrieveCard();
                        player.addCardToDeck(retrievedCard);
                        gameInterface.addCardToDeck(player, retrievedCard);
                    }
                }

                if(players.isEnd()) {
                    break;
                }
            }

            round++;
        }

        gameInterface.gameOver(players, round);
    }
}
