package io.techniktom.translations;

import io.techniktom.card.Card;
import io.techniktom.card.CardColor;

import java.util.Map;

public abstract class Translation {
    private final Map<Card, String> cardTranslations;
    private final Map<CardColor, String> cardColorTranslations;
    private final Map<String, String> stringTranslations;
    private final String internationalLanguageName;
    private final String languageName;

    protected Translation(String internationalLanguageName, String languageName) {
        this.cardTranslations = cardTranslation();
        this.stringTranslations = stringTranslation();
        this.cardColorTranslations = cardColorTranslation();
        this.internationalLanguageName = internationalLanguageName;
        this.languageName = languageName;
    }

    protected abstract Map<Card, String> cardTranslation();
    protected abstract Map<CardColor, String> cardColorTranslation();
    protected abstract Map<String, String> stringTranslation();

    public String getCardName(Card card) {
        return cardTranslations.get(card);
    }
    public String getCardColorName(CardColor card) {
        return cardColorTranslations.get(card);
    }
    public String getTranslation(String key) {
        return stringTranslations.get(key);
    }
}
