package io.techniktom.translations;

import io.techniktom.card.Card;
import io.techniktom.card.CardColor;

import java.util.Map;

public class EnglishTranslation extends Translation {

    public EnglishTranslation() {
        super("English", "English");
    }

    @Override
    protected Map<Card, String> cardTranslation() {
        return null;
    }

    @Override
    protected Map<CardColor, String> cardColorTranslation() {
        return null;
    }

    @Override
    protected Map<String, String> stringTranslation() {
        return null;
    }
}
