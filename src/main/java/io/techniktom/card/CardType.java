package io.techniktom.card;

public enum CardType implements Comparable<CardType> {
    SVRSEK, MENIC, KRAL, ESO, SEVEN, EIGHT, NINE, X
}
