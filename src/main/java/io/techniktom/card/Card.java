package io.techniktom.card;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class Card implements Comparable<Card> {
    private final CardColor color;
    private final CardType type;

    private Card(CardColor color, CardType type) {
        this.color = color;
        this.type = type;
    }

    private static boolean isCardTypeSuperSpecial(CardType type) {
        return Objects.equals(CardType.MENIC, type);
    }

    private static boolean isCardTypeSpecial(CardType type) {
        return Objects.equals(CardType.SEVEN, type)
            || Objects.equals(CardType.ESO, type);
    }

    public CardColor getColor() {
        return color;
    }

    public CardType getType() {
        return type;
    }

    public Boolean isSpecial() {
        return isCardTypeSpecial(type);
    }

    public Boolean isSuperSpecial() {
        return isCardTypeSuperSpecial(type);
    }

    @Override
    public int compareTo(Card card) {
        return Objects.equals(type, card.type) || Objects.equals(color, card.color) ? 0 : -1;
    }

    public static class Cards {
        private static final List<Card> allCards = createCards();

        private static List<Card> createCards() {
            var types = List.of(CardType.values());

            return Stream
                .of(CardColor.values())
                .flatMap(c -> types.stream().map(t -> new Card(c, t)))
                .toList();
        }

        public static List<Card> getAllCards() {
            return allCards;
        }
    }
}
